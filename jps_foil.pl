/***** Struktura programu wzorowana
  na implementacji algorytmu indukcji regu�
  W WERSJI DLA ZADANIA KLASYFIKACJI OBIEKT�W
  OPISANYCH WARTO�CIAMI ATRYBUT�W wed�ug

      Bratko    Prolog Programming for Artificial Intelligence
                ed. 3   Pearson Education / Addison-Wesley  2001

*******************************************************************/



/****Do testowych wykona� programu w ramach etapu 1
   �a wi�c przed skonstruowaniem procedury inicjalizuj�cej learn�
    nale�y poda� w wywo�aniu procedury learn_rules :

--jako przyk�ady pozytywne list� struktur o funktorze pos
--jako przyk�ady negatywne list� struktur o funktorze neg
--struktur� reprezentuj�c� nast�pnik w�a�ciwy dla predykatu, 
  kt�rego definicji ma uczy� si� program w tym przebiegu
	--np. wuj(x, y),  je�li przedmiotem uczenia ma by� predykat wuj
--indeks wykorzystania zmiennych w nast�pniku (w powy�szym przyk�adzie: 2).

Nale�y te� pami�ta� o wprowadzeniu do programu w postaci fakt�w:

--bazy fakt�w operacyjnych � fakty known_fact
--predykat�w wyst�puj�cych w faktach bazy wiedzy - fakty predicate
--listy symboli do wykorzystania jako zmienne - fakt variables

*******************************************************************/

learn( Rules):-
	writeln("Podaj nazwe predykatu do uczenia: "),
	read(PredName),
	initialize(),
	predicate(PredName, N),
	LastUsed is 0,
	our_findall(PosEx, find_pos(PredName,N,PosEx), PosExamples),
	writeln("Znalezlismy pozytywne przyklady."),
	read(_),
	our_findall(NegEx, find_neg(PredName,N,NegEx), NegExamples),
	writeln("Znalezlismy negatywne przyklady."),
	read(_),
	our_findall(X, insert_used(N, X), PredArgs),
	Predicate =.. [PredName| PredArgs],
	PredToDel =.. [predicate, PredName, N],
	retract(PredToDel),
	learn_rules(PosExamples, NegExamples, Predicate, N, 0, Rules).

append([], X, [X]).
append([Y|List], X, [Y|Result]):-
	append(List, X, Result).

initialize() :-
	retractall(variables(_)),
	retractall(predicate(_,_)),
	% assertz(variables([a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z])),
	assertz(variables([a, b])),
	assertz(predicate(syn,2)),
	%assertz(predicate(corka,2)),
	assertz(predicate(brat, 2)),
	assertz(predicate(kuzyn,2)).

find_pos(PredName, ArgLen, Expr):-
	known_fact(Expr), 
	Expr=..[PredName|Args], 
	length(Args, N), 
	N is ArgLen.
  
find_neg(PredName, ArgLen, Expr):-
	object_list(ObjList), 
	writeln("Generujemy permutacje argumentow dla przykladow negatywnych."),
	make_perm(ObjList, ArgLen, Args), 
	Expr=..[PredName|Args], 
	not(known_fact(Expr)).

object_list(ObjList):-
	our_findall(Expr, known_fact(Expr), ExprList),
	object_list_rec([], ExprList, ObjList).

object_list_rec(Res, [], Res).
object_list_rec(ObjList, [Expr|Rexpr], Result):-
	Expr=..[_|Objs],
	add_to_list(ObjList, Objs, ParRes), 
	object_list_rec(ParRes, Rexpr, Result).
  
add_to_list(List, [X|RestToAdd], [X|FilteredList]):-
	not(member1(X, List)), 
	not(member1(X, RestToAdd)), 
	add_to_list(List, RestToAdd, FilteredList).
add_to_list(List, [X|RestToAdd], FilteredList):-
	member1(X, RestToAdd), 
	add_to_list(List, RestToAdd, FilteredList).
add_to_list(List, [X|RestToAdd], FilteredList):-
	member1(X, List), 
	add_to_list(List, RestToAdd, FilteredList).
add_to_list(List, [], List).
  
make_perm(_, 0, []).
make_perm(ObjList, N, [X|Rest]):-
	N>0,
	member1(X, ObjList),
	delete(ObjList, X, NewObjList),
	N1 is N-1,
	make_perm(NewObjList, N1, Rest).
  
our_findall(X,  Goal,  Xlist) :-
    call(Goal),                          
    assertz(queue(X)),                  
    fail;                               
	assertz(queue(bottom)),     
    collect(Xlist).                     

collect([X|L])  :-
	retract(queue(X)),
	X \= bottom, !,               
	collect(L). 
collect([]).

sort_rules([ ], [ ]).
sort_rules([X|Rest], SortedList) :-
	split(X, Rest, Higher, Lower) ,
	sort_rules(Lower, SortedLower),
	sort_rules(Higher, SortedHigher) ,
	conc(SortedHigher, [X|SortedLower], SortedList).

split( _ , [ ], [ ], [ ]) :-
	!.
split(rule_descr(XRule,XScore,XLastUsed), [rule_descr(YRule,YScore,YLastUsed)|Rest], [rule_descr(YRule,YScore,YLastUsed)|Rest1], L2):-
	XScore =< YScore, !,
	split(rule_descr(XRule,XScore,XLastUsed), Rest, Rest1, L2) .
split(X, [Y|Rest], L1, [Y|Rest2]) :-
	split(X, Rest, L1, Rest2) .

conc([ ], L2, L2).
conc( [X|R1], L2, [X|RN]):-
	conc(R1, L2, RN).

choose([], _, _).

choose([rule_descr(Rule,Score,RetLastUsed)|_], Rule, RetLastUsed).

choose([_|RestRules], NewRule, RetLastUsed):-
	choose(RestRules, NewRule, RetLastUsed).

ruleLength(rule(_, List), Length):-
	length(List, Length).

/****************** KOD ORYGINALNY ******************/

learn_rules([ ] , _ , _ , _, _, [ ]).

learn_rules(PosExamples, NegExamples, Conseq, VarsIndex, Limit, [Rule | RestRules])  :-
	writeln("Rozpoczynamy uczenie sie regul."),
	read(_),
	learn_one_rule(PosExamples, NegExamples, rule(Conseq, [ ]), VarsIndex, Limit, Rule ) ,
	remove(PosExamples, Rule, RestPosExamples),
	learn_rules(RestPosExamples, NegExamples, Conseq, VarsIndex, Limit, RestRules).


learn_one_rule( _ , [ ] , Rule, _ , _, Rule).

learn_one_rule(PosExamples, NegExamples, PartialRule, LastUsed, Limit, Rule ) :-
  writeln("Rozpoczynamy uczenie sie jednej reguly."),
  read(_),
  new_partial_rule(PosExamples, NegExamples,PartialRule, LastUsed, NewPartialRule, NewLastUsed),
  ruleLength(NewPartialRule, NewPartialRuleLen),
  Limit > NewPartialRuleLen, !,
  filter(PosExamples, NewPartialRule, PosExamples1),
  filter(NegExamples, NewPartialRule, NegExamples1),
  learn_one_rule(PosExamples1, NegExamples1, NewPartialRule ,NewLastUsed, Limit, Rule ).

learn_one_rule(PosExamples, NegExamples, PartialRule, LastUsed, Limit, Rule ) :-
	writeln("Zwiekszamy limit i rozpoczynamy uczenie sie jednej reguly."),
 	read(_),
	NewLimit is Limit + 1,
	new_partial_rule(PosExamples, NegExamples,PartialRule, LastUsed, NewPartialRule, NewLastUsed),
	filter(PosExamples, NewPartialRule, PosExamples1),
    filter(NegExamples, NewPartialRule, NegExamples1),
	learn_one_rule(PosExamples1, NegExamples1, NewPartialRule,NewLastUsed, NewLimit, Rule ).

new_partial_rule(PosExamples, NegExamples, PartialRule, LastUsed, BestRule, RetLastUsed) :-
	writeln("Uczymy sie nowej reguly czastkowej i oceniamy ja."),
	read(_),
	our_findall(NewRuleDescr,scored_rule(PosExamples, NegExamples, PartialRule, LastUsed, NewRuleDescr), Rules),
	writeln("Sortujemy znalezione reguly czastkowe."),
	read(_),
	sort_rules(Rules, SortedRules),
	writeln("Wybieramy niedeterministycznie jedna regule z posortowanej listy."),
	read(_),
	our_choose(SortedRules, BestRule, RetLastUsed).

scored_rule(PosExamples, NegExamples, PartialRule, LastUsed, rule_descr(CandPartialRule, Score, RetLastUsed) ) :-
	candidate_rule(PartialRule, PosExamples, NegExamples, LastUsed, CandPartialRule, RetLastUsed) ,
	filter(PosExamples, CandPartialRule, PosExamples1),
    filter(NegExamples, CandPartialRule, NegExamples1),
    length(PosExamples1, NPos),
    length(NegExamples1, NNeg),
    NPos > 0,
	Score is NPos - NNeg.

our_choose( [rule_descr(Rule,_ ,RetLastUsed)],Rule, RetLastUsed).

our_choose( [rule_descr(Rule1,Score1,RetLastUsed1),rule_descr(_,Score2,_)|RestRules],
	                                                                             BestRule, RetLastUsed) :-

	Score1 > Score2, !,
	our_choose( [rule_descr(Rule1,Score1,RetLastUsed1)|RestRules], BestRule, RetLastUsed).

our_choose( [rule_descr(_,_,_),rule_descr(Rule2,Score2,RetLastUsed2)|RestRules],
	                                                                             BestRule, RetLastUsed) :-

	our_choose( [rule_descr(Rule2,Score2,RetLastUsed2)|RestRules], BestRule, RetLastUsed).


candidate_rule(rule(Conseq, Anteced), _ , NegExamples, LastUsed, rule(Conseq, [Expr|Anteced]), RetLastUsed) :-
	writeln("Konstruujemy nowa regule czastkowa (jedna z mozliwych)"),
	read(_),
	build_expr(LastUsed, Expr, RetLastUsed),
	suitable(rule(Conseq, [Expr|Anteced]), NegExamples).



build_expr(LastUsed,Expr,RetLastUsed) :-
	predicate(Pred, N),
	build_arg_list(N, vars(LastUsed, LastUsed), false, ArgList, RetLastUsed),
	Expr =.. [Pred|ArgList] .



build_arg_list(1, vars(LastUsed, LastLocal), true, [Arg], RetLastLocal) :-
	insert_arg(LastUsed, LastLocal,true, Arg, RetLastLocal, _).


build_arg_list(1, vars(LastUsed, LastLocal), false, [Arg], LastLocal) :-
	insert_used(LastUsed, Arg).



build_arg_list(N, vars(LastUsed, LastLocal), FlagIn, [Arg|RestArgs], RetLastLocal) :-
	writeln("Konstruujemy liste argumentow dla wyrazenia predykatowego."),
  	read(_),
	N>1,
	insert_arg(LastUsed, LastLocal,FlagIn, Arg, NewLastLocal, FlagOut),
	N1  is N-1,
	build_arg_list(N1, vars(LastUsed, NewLastLocal), FlagOut, RestArgs, RetLastLocal).



insert_arg(LastUsed, LastLocal, FlagIn, Arg, RetLastLocal, FlagOut) :-
	writeln("Generujemy jedna zmienna z listy."),
  	read(_),
	choose_var_index(LastUsed, LastLocal, FlagIn,Index,RetLastLocal, FlagOut),
	variables(Vars),
	length(Vars, L),
	Index > L,
	writeln("Skonczyly sie zmienne. Podaj nowa: "),
	read(N),
	append(Vars, N, NewVars),
	retract(variables(_)),
	assertz(variables(NewVars)),
	take_var(Index, NewVars, Arg).

insert_arg(LastUsed, LastLocal, FlagIn, Arg, RetLastLocal, FlagOut) :-
	choose_var_index(LastUsed, LastLocal, FlagIn,Index,RetLastLocal, FlagOut),
	variables(Vars),
	take_var(Index, Vars, Arg).

insert_used(LastUsed, Arg) :-
	generate_number(1, LastUsed, Index),
	variables(Vars),
	take_var(Index, Vars, Arg).


take_var(1,[Var|_], Var).

take_var(Index,[_|Rest], Arg) :-
	Index>1,
	Index1  is  Index-1,
	take_var(Index1,Rest, Arg).


choose_var_index(LastUsed, LastLocal,_,N,LastLocal, true) :-
	generate_number(1, LastUsed, N).

choose_var_index(LastUsed, LastLocal, FlagIn,N,LastLocal, FlagIn) :-
	Min is  LastUsed+1,
	generate_number(Min, LastLocal, N).

choose_var_index(_, LastLocal,FlagIn,N,N,FlagIn) :-
	N  is  LastLocal+1.


generate_number(Min,Max,Min):-
	Min=<Max.

generate_number(Min,Max,N):-
	Min<Max,
	NewMin is Min+1,
	generate_number(NewMin,Max,N).


filter(Examples, Rule, Examples1) :-
    findall(Example,(member1(Example, Examples), covers(Rule, Example)),Examples1).


remove([ ],_ ,[ ]).

remove([Example|Rest], Rule, Rest1) :-
	covers(Rule, Example), ! ,
	remove(Rest, Rule, Rest1).

remove([Example|Rest], Rule, [Example|Rest1]) :-
	remove(Rest, Rule, Rest1).



suitable(PartialRule, NegExamples) :-
	member1(NegEx, NegExamples),
	\+covers(PartialRule, NegEx).


covers(rule(Conseq, Anteced), Example) :-
     match_conseq(Conseq, Example, Bindings),
     match_anteced(Anteced, Bindings, _ ) .



match_conseq(Conseq, Example, BindingsOut) :-
    Conseq =.. [_|ArgList1],
    Example =.. [_|ArgList2],
    match_arg_lists(ArgList1,ArgList2,[],BindingsOut) .

match_anteced([ ], Bindings, Bindings) .

match_anteced([A|RestAnteced], BindingsIn, BindingsOut) :-
    match_expr(A, BindingsIn, Bindings1),
    match_anteced(RestAnteced, Bindings1, BindingsOut) .

match_expr(Expr,BindingsIn,BindingsOut) :-
    known_fact(Fact),
    functor(Expr,Functor,N),
    functor(Fact,Functor,N),
    Expr =.. [_|ArgList1],
    Fact =.. [_|ArgList2],
    match_arg_lists(ArgList1,ArgList2,BindingsIn,BindingsOut) .

match_arg_lists([ ] ,[ ], Bindings, Bindings) .

match_arg_lists([Arg1|Rest1], [Arg2|Rest2], BindingsIn, BindingsOut) :-
    match_args(Arg1, Arg2, BindingsIn, Bindings1),
    match_arg_lists(Rest1, Rest2, Bindings1, BindingsOut) .


match_args(Var,Val,[],[b(Var,Val)]).

match_args(Var,Val,[b(Var,Val)|RestBindings],[b(Var,Val)|RestBindings]).

match_args(Var,Val,[b(Var1,Val1)|RestBindings],[b(Var1,Val1)|RestBindings1]) :-
    Var\=Var1,  Val\=Val1,
    match_args(Var,Val,RestBindings,RestBindings1).



member1(X,[X|_]).
member1(X,[_|R]):-
    member1(X,R).


length1([ ], 0).

length1([_|Rest], N)  :-
	length1(Rest, NRest) ,
	N  is  NRest + 1 .


/******************	BAZA WIEDZY ***********************/


% known_fact(syn(janusz,anna)).
% known_fact(corka(anna,mariusz)).
% known_fact(wnuk(janusz,mariusz)).

% known_fact(syn(adrian,magda)).
% known_fact(corka(magda,michal)).
% known_fact(wnuk(adrian,michal)).

known_fact(syn(janusz,anna)).
known_fact(brat(adam, anna)).
known_fact(syn(tomek, adam)).
known_fact(kuzyn(janusz, tomek)).
 
known_fact(syn(patryk, ola)).
known_fact(brat(marcin, ola)).
known_fact(syn(jacek, marcin)).
known_fact(kuzyn(patryk, jacek)).

% known_fact(syn(adrian,magda)).
% known_fact(corka(magda,michal)).
% known_fact(wnuk(adrian,michal)).
